import 'package:meals_app/models/meal.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'favorites_provider.g.dart';

@Riverpod(keepAlive: true)
class FavoritesMeals extends _$FavoritesMeals {
  @override
  List<Meal> build() => [];

  bool toggleFavoriteMeal(Meal meal) {
    final exist = state.contains(meal);

    if (exist) {
      state = state.where((mealState) => mealState.id != meal.id).toList();
      return false;
    } else {
      state = [...state, meal];
      return true;
    }
  }
}
