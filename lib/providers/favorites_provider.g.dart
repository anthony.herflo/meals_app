// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorites_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$favoritesMealsHash() => r'ce1c6609c556f77f0b963fbaab8385b2fe4e710d';

/// See also [FavoritesMeals].
@ProviderFor(FavoritesMeals)
final favoritesMealsProvider =
    NotifierProvider<FavoritesMeals, List<Meal>>.internal(
  FavoritesMeals.new,
  name: r'favoritesMealsProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$favoritesMealsHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$FavoritesMeals = Notifier<List<Meal>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
