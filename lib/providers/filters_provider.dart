import 'package:meals_app/models/meal.dart';
import 'package:meals_app/providers/meals_provider.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'filters_provider.g.dart';

enum FilterMeal { glutenFree, lactoseFree, vegetarian, vegan }

@Riverpod(keepAlive: true)
class FilterMeals extends _$FilterMeals {
  @override
  Map<FilterMeal, bool> build() {
    print('FilterMeals: build invoked');
    return {
      FilterMeal.glutenFree: false,
      FilterMeal.lactoseFree: false,
      FilterMeal.vegetarian: false,
      FilterMeal.vegan: false,
    };
  }

  void setFilter(FilterMeal filter, bool isActive) {
    // if (state case Map<FilterMeal, bool> existingState!)
    state = {...state, filter: isActive};
  }
}

@riverpod
List<Meal> filteredMealsList(FilteredMealsListRef ref) {
  final meals = ref.watch(allMealsProvider);
  final filters = ref.watch(filterMealsProvider);
  final filteredMeals = [...meals];

  if (filters[FilterMeal.glutenFree]!) {
    filteredMeals.removeWhere((meal) => meal.isGlutenFree != true);
  }
  if (filters[FilterMeal.lactoseFree]!) {
    filteredMeals.removeWhere((meal) => meal.isLactoseFree != true);
  }
  if (filters[FilterMeal.vegetarian]!) {
    filteredMeals.removeWhere((meal) => meal.isVegetarian != true);
  }
  if (filters[FilterMeal.vegan]!) {
    filteredMeals.removeWhere((meal) => meal.isVegan != true);
  }

  return filteredMeals;
}
