// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filters_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$filteredMealsListHash() => r'ab9eecad6ef92afc0caf6d739338b9e3d2cdebe9';

/// See also [filteredMealsList].
@ProviderFor(filteredMealsList)
final filteredMealsListProvider = AutoDisposeProvider<List<Meal>>.internal(
  filteredMealsList,
  name: r'filteredMealsListProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$filteredMealsListHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef FilteredMealsListRef = AutoDisposeProviderRef<List<Meal>>;
String _$filterMealsHash() => r'add0d53d7d9b6881d9939b771b29f8df387cd803';

/// See also [FilterMeals].
@ProviderFor(FilterMeals)
final filterMealsProvider =
    NotifierProvider<FilterMeals, Map<FilterMeal, bool>>.internal(
  FilterMeals.new,
  name: r'filterMealsProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$filterMealsHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$FilterMeals = Notifier<Map<FilterMeal, bool>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
