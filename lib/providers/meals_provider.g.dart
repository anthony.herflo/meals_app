// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meals_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$allMealsHash() => r'cff7ae9c0378ed7267098befc87fe346dbf58bda';

/// See also [allMeals].
@ProviderFor(allMeals)
final allMealsProvider = AutoDisposeProvider<List<Meal>>.internal(
  allMeals,
  name: r'allMealsProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$allMealsHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef AllMealsRef = AutoDisposeProviderRef<List<Meal>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
