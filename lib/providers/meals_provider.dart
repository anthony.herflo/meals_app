import 'package:meals_app/data/fake_meals.dart';
import 'package:meals_app/models/meal.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'meals_provider.g.dart';

@riverpod
List<Meal> allMeals(AllMealsRef ref) => dummyMeals;
