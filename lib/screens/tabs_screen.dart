import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meals_app/providers/favorites_provider.dart';
import 'package:meals_app/screens/categories_screen.dart';
import 'package:meals_app/screens/filters_screen.dart';
import 'package:meals_app/screens/meals_screen.dart';
import 'package:meals_app/utils/typedefs.dart';
import 'package:meals_app/widgets/main_drawer.dart';

class TabsScreen extends ConsumerStatefulWidget {
  const TabsScreen({super.key});

  static String routeName = '/tabs';
  static MaterialPageRoute pageRoute() =>
      MaterialPageRoute(builder: (context) => const TabsScreen());

  @override
  ConsumerState<TabsScreen> createState() => _TabsScreenState();
}

class _TabsScreenState extends ConsumerState<TabsScreen> {
  int _currentPageIndex = 0;

  void _showSnackBackMessage(String message) {}

  void _selectPage(int index) => setState(() => _currentPageIndex = index);

  void _onDrawerItemTap(String identifierScreen) {
    Navigator.of(context).pop();
    if (identifierScreen == 'filters') {
      Navigator.of(context).pushReplacement(FiltersScreen.pageRoute());
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final favoriteMeals = ref.watch(favoritesMealsProvider);

    final List<PagesRecord> pages = [
      (title: 'Categories', body: const CategoriesScreen()),
      (
        title: 'Your Favorites',
        body: MealsListContent(meals: favoriteMeals),
      )
    ];
    final page = pages[_currentPageIndex];

    return Scaffold(
      drawer: MainDrawer(onListTileSelected: _onDrawerItemTap),
      appBar: AppBar(
        title: Text(page.title),
      ),
      body: page.body,
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(
              icon: Icon(Icons.set_meal), label: 'Categories'),
          BottomNavigationBarItem(icon: Icon(Icons.star), label: 'Favorites'),
        ],
        onTap: _selectPage,
        currentIndex: _currentPageIndex,
      ),
    );
  }
}
