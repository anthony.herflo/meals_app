import 'package:flutter/material.dart';
import 'package:meals_app/models/meal.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meals_app/providers/favorites_provider.dart';

class MealDetailScreen extends ConsumerWidget {
  const MealDetailScreen({
    super.key,
    required this.meal,
  });

  static String routeName = '/meal';
  static MaterialPageRoute route(Meal meal) => MaterialPageRoute(
        builder: (context) => MealDetailScreen(meal: meal),
      );

  final Meal meal;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final favoritesMeals = ref.watch(favoritesMealsProvider);
    final wasAdded = favoritesMeals.contains(meal);

    return Scaffold(
      appBar: AppBar(
        title: Text(meal.title),
        actions: [
          IconButton(
            onPressed: () {
              final wasAdded = ref
                  .read(favoritesMealsProvider.notifier)
                  .toggleFavoriteMeal(meal);

              ScaffoldMessenger.of(context).clearSnackBars();
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(
                      '${meal.title} was ${wasAdded ? "Added to" : "Removed from"} your favorites'),
                ),
              );
            },
            icon: AnimatedSwitcher(
              duration: const Duration(milliseconds: 400),
              switchInCurve: Curves.easeInOutCirc,
              switchOutCurve: Curves.easeInOutCirc,
              transitionBuilder: (child, animation) => ScaleTransition(
                scale: Tween<double>(begin: 0.2, end: 1).animate(animation),
                child: child,
              ),
              child: wasAdded
                  ? const Icon(
                      key: ValueKey(true),
                      Icons.favorite,
                    )
                  : const Icon(
                      key: ValueKey(false),
                      Icons.favorite_outline,
                    ),
            ),
          ),
        ],
      ),
      body: ListView(
        children: [
          Hero(
            tag: meal.id,
            child: Image.network(
              meal.imageUrl,
              height: 300,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(height: 10),
                const _SubTitle(text: 'Ingredients'),
                const SizedBox(height: 4),
                for (final ingredient in meal.ingredients)
                  Text(
                    ingredient,
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                const SizedBox(height: 10),
                const _SubTitle(text: 'Steps'),
                const SizedBox(height: 4),
                for (final step in meal.steps)
                  Text(
                    '${meal.steps.indexOf(step) + 1}.- $step',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyMedium,
                  )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _SubTitle extends StatelessWidget {
  const _SubTitle({required this.text});
  final String text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: TextAlign.center,
      style: Theme.of(context)
          .textTheme
          .labelMedium
          ?.copyWith(color: Theme.of(context).colorScheme.onSurfaceVariant),
    );
  }
}
