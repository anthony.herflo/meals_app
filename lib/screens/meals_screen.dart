import 'package:flutter/material.dart';
import 'package:meals_app/models/meal.dart';
import 'package:meals_app/screens/meal_detail_screen.dart';
import 'package:meals_app/widgets/meal_list_item.dart';

class MealsScreen extends StatelessWidget {
  const MealsScreen({
    super.key,
    required this.title,
    required this.meals,
  });

  final String title;
  final List<Meal> meals;

  static const routeName = '/meals';

  static MaterialPageRoute pageRoute({
    required String title,
    required List<Meal> meals,
  }) =>
      MaterialPageRoute(
        builder: (context) => MealsScreen(title: title, meals: meals),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: MealsListContent(meals: meals),
    );
  }
}

class MealsListContent extends StatelessWidget {
  const MealsListContent({
    super.key,
    required this.meals,
  });
  final List<Meal> meals;

  @override
  Widget build(BuildContext context) {
    return meals.isEmpty
        ? const _NothingHereContainer()
        : ListView.builder(
            itemCount: meals.length,
            padding: const EdgeInsets.all(8.0),
            itemBuilder: (context, index) => MealListItem(
              margin: const EdgeInsets.all(4.5),
              onTap: () {
                Navigator.of(context)
                    .push(MealDetailScreen.route(meals[index]));
              },
              meal: meals[index],
            ),
          );
  }
}

class _NothingHereContainer extends StatelessWidget {
  const _NothingHereContainer();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'Uh nothing here.',
        style: Theme.of(context).textTheme.headlineMedium,
      ),
    );
  }
}
