import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meals_app/providers/filters_provider.dart';
import 'package:meals_app/screens/tabs_screen.dart';
import 'package:meals_app/widgets/main_drawer.dart';

class FiltersScreen extends ConsumerWidget {
  const FiltersScreen({super.key});

  static const String routeName = '/filters';
  static MaterialPageRoute pageRoute() =>
      MaterialPageRoute(builder: (context) => const FiltersScreen());

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final activeFilters = ref.watch(filterMealsProvider);

    return Scaffold(
      drawer: MainDrawer(onListTileSelected: (identifierScreen) {
        Navigator.of(context).pop();
        if (identifierScreen == 'tabs') {
          Navigator.of(context).pushReplacement(TabsScreen.pageRoute());
        }
      }),
      appBar: AppBar(
        title: const Text('Your Filters'),
      ),
      body: Column(
        children: [
          _SwitchValue(
            title: 'Gluten-Free',
            subTitle: 'Only include the gluten-free meals',
            value: activeFilters[FilterMeal.glutenFree] ?? false,
            onChange: (value) => ref
                .read(filterMealsProvider.notifier)
                .setFilter(FilterMeal.glutenFree, value),
          ),
          _SwitchValue(
            title: 'Lactose-Free',
            subTitle: 'Only include the lactose-free meals',
            value: activeFilters[FilterMeal.lactoseFree] ?? false,
            onChange: (value) => ref
                .read(filterMealsProvider.notifier)
                .setFilter(FilterMeal.lactoseFree, value),
          ),
          _SwitchValue(
            title: 'Vegetarian',
            subTitle: 'Only include the vegetarian meals',
            value: activeFilters[FilterMeal.vegetarian] ?? false,
            onChange: (value) => ref
                .read(filterMealsProvider.notifier)
                .setFilter(FilterMeal.vegetarian, value),
          ),
          _SwitchValue(
            title: 'Vegan',
            subTitle: 'Only include the vegan meals',
            value: activeFilters[FilterMeal.vegan] ?? false,
            onChange: (value) => ref
                .read(filterMealsProvider.notifier)
                .setFilter(FilterMeal.vegan, value),
          ),
        ],
      ),
    );
  }
}

class _SwitchValue extends StatelessWidget {
  const _SwitchValue({
    required this.title,
    required this.subTitle,
    required this.value,
    required this.onChange,
  });

  final String title;
  final String subTitle;
  final bool value;
  final void Function(bool) onChange;

  @override
  Widget build(BuildContext context) {
    return SwitchListTile.adaptive(
      value: value,
      onChanged: onChange,
      title: Text(
        title,
        style: Theme.of(context).textTheme.titleLarge,
      ),
      subtitle: Text(
        subTitle,
        style: Theme.of(context).textTheme.titleSmall,
      ),
      activeColor: Theme.of(context).colorScheme.tertiary,
      contentPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
    );
  }
}
