import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meals_app/data/fake_categories.dart';
import 'package:meals_app/models/category.dart';
import 'package:meals_app/models/meal.dart';
import 'package:meals_app/providers/filters_provider.dart';
import 'package:meals_app/screens/meals_screen.dart';
import 'package:meals_app/utils/typedefs.dart';
import 'package:meals_app/widgets/category_grid_item.dart';

class CategoriesScreen extends ConsumerStatefulWidget {
  const CategoriesScreen({super.key});

  static const routeName = '/categories';
  static MaterialPageRoute pageRoute(OnMealFunction onMealToggle) =>
      MaterialPageRoute(
        builder: (context) => const CategoriesScreen(),
      );

  @override
  ConsumerState<CategoriesScreen> createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends ConsumerState<CategoriesScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 250),
      lowerBound: 0,
      upperBound: 1,
    );

    _animationController.forward();

    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();

    super.dispose();
  }

  void _onCategoryClick(
      BuildContext context, Category category, List<Meal> meals) {
    final categoryMeals =
        meals.where((meal) => meal.categories.contains(category.id)).toList();
    Navigator.of(context).push(
      MealsScreen.pageRoute(title: category.title, meals: categoryMeals),
    );
  }

  @override
  Widget build(BuildContext context) {
    final filteredMeals = ref.watch(filteredMealsListProvider);
    final crossCount = MediaQuery.of(context).size.width < 480 ? 2 : 3;

    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) => SlideTransition(
        position: Tween(
          begin: const Offset(0, 0.3),
          end: const Offset(0, 0),
        ).animate(
          CurvedAnimation(parent: _animationController, curve: Curves.easeIn),
        ),
        child: child,
      ),
      child: GridView(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.5),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: crossCount,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 15,
            mainAxisSpacing: 20,
          ),
          children: [
            for (final category in availableCategories)
              CategoryGridItem(
                category: category,
                onTap: () {
                  _onCategoryClick(context, category, filteredMeals);
                },
              )
          ]),
    );
  }
}
