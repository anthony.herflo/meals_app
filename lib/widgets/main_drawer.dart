import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  const MainDrawer({super.key, required this.onListTileSelected});

  final void Function(String) onListTileSelected;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(children: [
        DrawerHeader(
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Theme.of(context).colorScheme.secondary,
                Theme.of(context).colorScheme.secondary.withOpacity(0.4),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
          child: Row(
            children: [
              Icon(
                Icons.fastfood,
                size: 48,
                color: Theme.of(context).colorScheme.onSecondary,
              ),
              const SizedBox(width: 18),
              Text(
                'Cooking Up',
                style: Theme.of(context).textTheme.displayLarge?.copyWith(
                    color: Theme.of(context).colorScheme.onSecondaryContainer),
              )
            ],
          ),
        ),
        ListTile(
          leading: const Icon(Icons.restaurant),
          title: Text(
            'Meals',
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          onTap: () {
            onListTileSelected('tabs');
          },
        ),
        ListTile(
          leading: const Icon(Icons.settings),
          title: Text(
            'Filters',
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          onTap: () {
            onListTileSelected('filters');
          },
        ),
      ]),
    );
  }
}
