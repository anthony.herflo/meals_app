import 'package:flutter/material.dart';

class MealItemTrait extends StatelessWidget {
  const MealItemTrait({
    super.key,
    required this.icon,
    required this.label,
    this.gap = 4.5,
  });

  final IconData icon;
  final String label;
  final double gap;

  @override
  Widget build(BuildContext context) {
    const iconSize = 18.0;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          icon,
          size: iconSize,
          color: Theme.of(context).colorScheme.onTertiaryContainer,
        ),
        SizedBox(width: gap),
        Text(
          label,
          style: Theme.of(context).textTheme.labelMedium,
        ),
      ],
    );
  }
}
