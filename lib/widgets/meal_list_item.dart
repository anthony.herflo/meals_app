import 'package:flutter/material.dart';
import 'package:meals_app/models/meal.dart';
import 'package:meals_app/widgets/meal_item_trait.dart';
import 'package:transparent_image/transparent_image.dart';

class MealListItem extends StatelessWidget {
  const MealListItem({
    super.key,
    required this.meal,
    required this.onTap,
    this.margin,
  });

  final Meal meal;
  final VoidCallback onTap;
  final EdgeInsets? margin;

  String _toSentenceFormat(String str) =>
      str[0].toUpperCase() + str.substring(1);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: margin,
      color: Theme.of(context).cardColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      clipBehavior: Clip.hardEdge,
      child: InkWell(
        onTap: onTap,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Hero(
              tag: meal.id,
              child: FadeInImage.memoryNetwork(
                image: meal.imageUrl,
                width: double.infinity,
                height: 180,
                fit: BoxFit.cover,
                placeholder: kTransparentImage,
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                padding: const EdgeInsets.all(8),
                color: Theme.of(context).colorScheme.surface.withOpacity(0.7),
                child: Column(children: [
                  Text(
                    meal.title,
                    style: Theme.of(context).textTheme.headlineMedium,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    softWrap: true,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      MealItemTrait(
                        icon: Icons.schedule,
                        label: '${meal.duration}min',
                      ),
                      MealItemTrait(
                        icon: Icons.work,
                        label: _toSentenceFormat(meal.complexity.name),
                      ),
                      MealItemTrait(
                        icon: Icons.money,
                        label: _toSentenceFormat(meal.affordability.name),
                      ),
                    ],
                  )
                ]),
              ),
            )
          ],
        ),
      ),
    );
  }
}
