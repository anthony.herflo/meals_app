import 'package:flutter/material.dart';
import 'package:meals_app/models/meal.dart';

typedef OnMealFunction = void Function(Meal meal);

typedef PagesRecord = ({String title, Widget body});
