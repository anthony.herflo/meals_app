import 'package:flutter/material.dart';
import 'package:meals_app/themes/text_theme.dart';

const _lightColorScheme = ColorScheme(
  brightness: Brightness.light,
  primary: Color(0xFFB7221F),
  onPrimary: Color(0xFFFFFFFF),
  primaryContainer: Color(0xFFFFDAD6),
  onPrimaryContainer: Color(0xFF410002),
  secondary: Color(0xFF006A68),
  onSecondary: Color(0xFFFFFFFF),
  secondaryContainer: Color(0xFF6FF7F4),
  onSecondaryContainer: Color(0xFF00201F),
  tertiary: Color(0xFF865300),
  onTertiary: Color(0xFFFFFFFF),
  tertiaryContainer: Color(0xFFFFDDB9),
  onTertiaryContainer: Color(0xFF2B1700),
  error: Color(0xFFBA1A1A),
  errorContainer: Color(0xFFFFDAD6),
  onError: Color(0xFFFFFFFF),
  onErrorContainer: Color(0xFF410002),
  surface: Color(0xFFFFFBFF),
  onSurface: Color(0xFF1B0261),
  surfaceContainerHighest: Color(0xFFF5DDDA),
  onSurfaceVariant: Color(0xFF534341),
  outline: Color(0xFF857371),
  onInverseSurface: Color(0xFFF4EEFF),
  inverseSurface: Color(0xFF302175),
  inversePrimary: Color(0xFFFFB4AB),
  shadow: Color(0xFF000000),
  surfaceTint: Color(0xFFB7221F),
  outlineVariant: Color(0xFFD8C2BF),
  scrim: Color(0xFF000000),
);

const _darkColorScheme = ColorScheme(
  brightness: Brightness.dark,
  primary: Color(0xFFFFB4AB),
  onPrimary: Color(0xFF690005),
  primaryContainer: Color(0xFF93000A),
  onPrimaryContainer: Color(0xFFFFDAD6),
  secondary: Color(0xFF4DDAD8),
  onSecondary: Color(0xFF003736),
  secondaryContainer: Color(0xFF00504F),
  onSecondaryContainer: Color(0xFF6FF7F4),
  tertiary: Color(0xFFFFB962),
  onTertiary: Color(0xFF472A00),
  tertiaryContainer: Color(0xFF663E00),
  onTertiaryContainer: Color(0xFFFFDDB9),
  error: Color(0xFFFFB4AB),
  errorContainer: Color(0xFF93000A),
  onError: Color(0xFF690005),
  onErrorContainer: Color(0xFFFFDAD6),
  surface: Color(0xFF1B0261),
  onSurface: Color(0xFFE5DEFF),
  surfaceContainerHighest: Color(0xFF534341),
  onSurfaceVariant: Color(0xFFD8C2BF),
  outline: Color(0xFFA08C8A),
  onInverseSurface: Color(0xFF1B0261),
  inverseSurface: Color(0xFFE5DEFF),
  inversePrimary: Color(0xFFB7221F),
  shadow: Color(0xFF000000),
  surfaceTint: Color(0xFFFFB4AB),
  outlineVariant: Color(0xFF534341),
  scrim: Color(0xFF000000),
);

class MealsTheme {
  ThemeData get lightData => ThemeData.from(
        colorScheme: _lightColorScheme,
        useMaterial3: true,
        textTheme: mealsTextTheme,
      );

  ThemeData get darkData => ThemeData.from(
        colorScheme: _darkColorScheme,
        useMaterial3: true,
        textTheme: mealsTextTheme,
      );
}
