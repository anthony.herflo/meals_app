import 'package:flutter/material.dart';

TextTheme get mealsTextTheme {
  const familyOne = 'AlbertSans';
  const familyTwo = 'Murecho';

  const defaultSize = 12.5;
  const multiplierLess = 0.9;
  const multiplierMore = 1.25;

  const displaySize = defaultSize * 1.9;
  const headlineSize = defaultSize * 1.6;
  const titleSize = defaultSize * 1.4;
  const labelSize = defaultSize * 1.2;

  return const TextTheme(
    displayLarge: TextStyle(
      fontFamily: familyOne,
      fontSize: displaySize * multiplierMore,
    ),
    displayMedium: TextStyle(
      fontFamily: familyOne,
      fontSize: displaySize,
    ),
    displaySmall: TextStyle(
      fontFamily: familyTwo,
      fontSize: displaySize * multiplierLess,
    ),
    headlineLarge: TextStyle(
      fontFamily: familyOne,
      fontSize: headlineSize * multiplierMore,
    ),
    headlineMedium: TextStyle(
      fontFamily: familyOne,
      fontSize: headlineSize,
    ),
    headlineSmall: TextStyle(
      fontFamily: familyTwo,
      fontSize: headlineSize * multiplierLess,
    ),
    titleLarge: TextStyle(
      fontFamily: familyOne,
      fontSize: titleSize * multiplierMore,
    ),
    titleMedium: TextStyle(
      fontFamily: familyOne,
      fontSize: titleSize,
    ),
    titleSmall: TextStyle(
      fontFamily: familyTwo,
      fontSize: titleSize * multiplierLess,
    ),
    labelLarge: TextStyle(
      fontFamily: familyOne,
      fontSize: labelSize * multiplierMore,
    ),
    labelMedium: TextStyle(
      fontFamily: familyTwo,
      fontSize: labelSize,
    ),
    labelSmall: TextStyle(
      fontFamily: familyTwo,
      fontSize: labelSize * multiplierLess,
    ),
    bodyLarge: TextStyle(
      fontFamily: familyOne,
      fontSize: defaultSize * multiplierMore,
    ),
    bodyMedium: TextStyle(
      fontFamily: familyTwo,
      fontSize: defaultSize,
    ),
    bodySmall: TextStyle(
      fontFamily: familyTwo,
      fontSize: defaultSize * multiplierLess,
    ),
  );
}
