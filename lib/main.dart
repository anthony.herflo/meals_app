import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meals_app/screens/tabs_screen.dart';
import 'package:meals_app/themes/meals_theme.dart';

void main(List<String> args) {
  runApp(
    const ProviderScope(child: MyApp()),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = MealsTheme();

    return MaterialApp(
      theme: theme.lightData,
      darkTheme: theme.darkData,
      home: const TabsScreen(),
    );
  }
}
